package AtividadeImperativa;
/**
 * Jo�o Felipe Fonseca Nascimento
 * 20161LBSI120055
 * Programa��o Orientada a Objetos
 * Emails: joao.nascimento98@academico.ifs.edu.br;
 * thecurrentuser@live.com
 */

//Ler a idade de 50 homens e 50 mulheres e armazenar em 2 vetores;
//Calcular a m�dia das idades dos homens e das mulheres;
//Verificar quantos homens tem a idade maior que a m�dia da idade das mulheres;
//Verificar quantas mulheres tem a idade maior que a m�dia da idade dos homens.

import java.util.*;

public class AtividadeImperativa {
	public static void main(String[] args) {
		Scanner read = new Scanner(System.in);

		int mediaHomens = 0;
		int mediaMulheres = 0;
		int aux = 0;
		int aux1 = 0;
		int quantidadeHomens = 0;
		int quantidadeMulheres = 0;

		int[] idadesHomens = new int[50];
		int[] idadesMulheres = new int[50];

		for (int i = 0; i < idadesMulheres.length; i++) {
			System.out.println("Digite a sua idade (Homem): ");
			idadesHomens[i] = read.nextInt();
			aux = aux + idadesHomens[i];
			System.out.println("Digite a sua idade (Mulher): ");
			idadesMulheres[i] = read.nextInt();
			aux1 = aux1 + idadesMulheres[i];
		}

		mediaHomens = aux / 50;
		mediaMulheres = aux1 / 50;

		for (int i = 0; i < idadesMulheres.length; i++) {
			if (idadesHomens[i] > mediaMulheres) {
				quantidadeHomens++;
			}
			if (idadesMulheres[i] > mediaHomens) {
				quantidadeMulheres++;
			}
		}
		System.out.printf(
				"%d Homens tem a idade maior que a m�dia feminina."
						+ "\n%d Mulheres tem a idade maior que a m�dia masculina",
				quantidadeHomens, quantidadeMulheres);
	}
}
